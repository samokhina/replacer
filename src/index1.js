const request = require("request");
const parser = require("fast-xml-parser");
const { interval, forkJoin, Observable } = require("rxjs");
const { takeWhile, tap, flatMap, first, last } = require("rxjs/operators");
const fs = require("fs");
const http = require("http");
const express = require("express"),
  app = express();
const mysql = require("mysql2");

const sql =
  "INSERT INTO app_review(userId, bookId, reviewId, rating) VALUES(?, ?, ?, ?)";

const connection = mysql.createConnection({
  host: "localhost",
  user: "root",
  database: "recommender",
  password: "1357",
});
connection.connect(function (err) {
  if (err) {
    return console.error("Ошибка: " + err.message);
  } else {
    console.log("Подключение к серверу MySQL успешно установлено");
  }
});

const host = "127.0.0.1";
const port = 3000;

app.get("/", (req, res) => {
  initRequests$
    .pipe(flatMap(() => reviews$))
    .pipe(last())
    .subscribe((reviews) => {
      // todo если убрать last() приходит почему то поочереди 4 раза
      // пока не понятно 4 раза он отправлет запросы или нет
      // во всяком случае за счет этого работает почему то дольше
      // как проверить отправляется ли каждый раз заново запросы или нет
      reviews = reviews.filter((review) => review != null);
      console.log("reviews: ", reviews);
      reviews.forEach((review) => {
        connection.query(sql, review, function (err, results) {
          if (err) console.log(err);
          else console.log("Данные добавлены");
        });
      });

      // fs.writeFileSync(__dirname + "/data.json", JSON.stringify(reviews));
      return res.send(JSON.stringify(reviews));
    });
});

app.listen(port, host, () =>
  console.log(`Server listens http://${host}:${port}`)
);

let ratings$ = [];
const initRequests$ = interval(500).pipe(
  takeWhile((value) => value < 5, true),
  tap((value) => {
    ratings$.push(getReview(value));
  })
);

const reviews$ = forkJoin(ratings$);

const urlReview =
  "https://www.goodreads.com/review/show.xml?key=zGsFwkC2OYSH8TiMofNg&id=";

function getReview(id) {
  return Observable.create((observer) => {
    request(urlReview + id, options, function (error, response, body) {
      if (parser.validate(body) === true) {
        const jsonObj = parser.parse(body, options);
        if (!jsonObj.error && !jsonObj.GoodreadsResponse.error) {
          const reviewJson = jsonObj.GoodreadsResponse.review;
          // const review = Rating.from({
          //   id: reviewJson.id,
          //   userId: reviewJson.user.id,
          //   itemId: reviewJson.book.id,
          //   rating: reviewJson.rating,
          // });
          const review = [
            reviewJson.user.id,
            reviewJson.book.id,
            reviewJson.id,
            reviewJson.rating,
          ];
          observer.next(review);
          // observer.complete();
        } else {
          observer.next(null);
          // observer.complete();
        }
      }
    });
  }).pipe(first());
}

const options = {
  attributeNamePrefix: "@_",
  attrNodeName: "attr",
  textNodeName: "#text",
  ignoreAttributes: true,
  ignoreNameSpace: false,
  allowBooleanAttributes: false,
  parseNodeValue: true,
  parseAttributeValue: false,
  trimValues: true,
  cdataTagName: "__cdata",
  cdataPositionChar: "\\c",
  parseTrueNumberOnly: false,
  arrayMode: false,
  stopNodes: ["parse-me-as-string"],
};

// class Rating {
//   userId;
//   itemId;
//   reviewId;
//   rating;
//
//   static from(json) {
//     return Object.assign(new Rating(), json);
//   }
// }
