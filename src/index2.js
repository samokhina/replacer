const request = require("request");
const parser = require("fast-xml-parser");
const { Observable, range, of } = require("rxjs");
const {
  first,
  concatMap,
  mergeMap,
  bufferCount,
  catchError,
  delay,
} = require("rxjs/operators");
const mysql = require("mysql2");

const sql =
  "INSERT INTO train_review(userId, bookId, reviewId, rating) VALUES(?, ?, ?, ?)";

const connection = mysql.createConnection({
  host: "localhost",
  user: "root",
  database: "recommendations",
  password: "1357",
});
connection.connect(function (err) {
  if (err) {
    return console.error("Ошибка: " + err.message);
  } else {
    console.log("Подключение к серверу MySQL успешно установлено");
  }
});

const options = {
  attributeNamePrefix: "@_",
  attrNodeName: "attr",
  textNodeName: "#text",
  ignoreAttributes: true,
  ignoreNameSpace: false,
  allowBooleanAttributes: false,
  parseNodeValue: true,
  parseAttributeValue: false,
  trimValues: true,
  cdataTagName: "__cdata",
  cdataPositionChar: "\\c",
  parseTrueNumberOnly: false,
  arrayMode: false,
  stopNodes: ["parse-me-as-string"],
};
const urlReview =
  "https://www.goodreads.com/review/show.xml?key=zGsFwkC2OYSH8TiMofNg&id=";

range(222651, 300000)
  .pipe(
    bufferCount(50),
    concatMap((values) => of(...values).pipe(delay(1500))),
    mergeMap((value) => getReview(value)),
    bufferCount(50)
  )
  .subscribe(
    (reviews) => {
      reviews = reviews.filter((review) => review != null);
      // console.log("reviews: ", reviews);
      reviews.forEach((review) => {
        connection.query(sql, review, function (err, results) {
          if (err) console.log("err: ", err);
          else console.log("Данные добавлены");
        });
      });

      // if (review !== null) {
      //   connection.query(sql, review, function (err, results) {
      //     if (err) console.log(err);
      //     else console.log("Данные добавлены");
      //   });
      // }
    },
    null,
    () => {
      // connection.close();
    }
  );

function getReview(id) {
  console.log("id: ", id);
  return Observable.create((observer) => {
    request(urlReview + id, options, function (error, response, body) {
      if (parser.validate(body) === true) {
        const jsonObj = parser.parse(body, options);
        if (
          !jsonObj.error &&
          !(jsonObj.GoodreadsResponse && jsonObj.GoodreadsResponse.error)
        ) {
          const reviewJson = jsonObj.GoodreadsResponse.review;
          if (+reviewJson.user.id < 50000 && +reviewJson.book.id < 100000) {
            const review = [
              reviewJson.user.id,
              reviewJson.book.id,
              id,
              reviewJson.rating,
            ];
            observer.next(review);
            // observer.complete();
          } else {
            observer.next(null);
          }
        } else {
          observer.next(null);
          // observer.complete();
        }
      }
    });
  }).pipe(
    first(),
    catchError(() => of(null))
  );
}
